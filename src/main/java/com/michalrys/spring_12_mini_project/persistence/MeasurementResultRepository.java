package com.michalrys.spring_12_mini_project.persistence;

import org.springframework.data.repository.CrudRepository;

public interface MeasurementResultRepository extends CrudRepository<MeasurementResult, Long> {
}
