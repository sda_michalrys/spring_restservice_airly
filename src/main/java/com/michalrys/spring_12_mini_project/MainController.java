package com.michalrys.spring_12_mini_project;

import com.michalrys.spring_12_mini_project.persistence.MeasurementResult;
import com.michalrys.spring_12_mini_project.persistence.MeasurementResultRepository;
import com.michalrys.spring_12_mini_project.services.GeocodingService;
import com.michalrys.spring_12_mini_project.services.MeasurementDataBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class MainController {

    @Autowired
    GeocodingService geocodingService;
    @Autowired
    MeasurementResultRepository measurementResultRepository;
    @Autowired
    MeasurementDataBuilder measurementDataBuilder;


    @GetMapping("/test")
    public MeasurementResult hello(@RequestParam("city") String cityName) {
        //http://localhost:8080/test?city=katowice
        Map<String, Double> results = geocodingService.geocode(cityName);
        MeasurementResult resultEntity = measurementDataBuilder.buildDataEntity(results);
        measurementResultRepository.save(resultEntity);
        return resultEntity;
    }
}
