package com.michalrys.spring_12_mini_project.services;

import com.michalrys.spring_12_mini_project.persistence.MeasurementResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;

@Service
public class GeocodingService {

    @Value("${geocoding.url}")
    private String url;

    @Value("${geocoding.key}")
    private String apiKey;

    @Autowired
    RestTemplate restTemplate;

    public Map<String, Double> geocode(String cityName) {
        String stations = "v2/measurements/installation?installationId=204";
        HttpHeaders headers = new HttpHeaders();
        headers.set("apikey", apiKey);

        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<Map> response = restTemplate.exchange(String.format("%s/%s", url, stations),
                HttpMethod.GET, entity, Map.class);

        List tempResult = (List) ((Map) (response.getBody().get("current"))).get("values");

        Map<String, Double> result = new HashMap<>();

        int i = 0;
        List<String> names = new ArrayList<>();

        Iterator<Map> iterator1 = tempResult.iterator();
        while (iterator1.hasNext()) {
            Map map = iterator1.next();

            Iterator<Map.Entry<String, Object>> iterator = map.entrySet().iterator();
            while (iterator.hasNext()) {
                Map.Entry<String, Object> next = iterator.next();
                 names.add(i, next.getValue().toString());

                if(i == 1) {
                    result.put(names.get(0), Double.valueOf(names.get(1)));
                    i = 0;
                    continue;
                }
                i++;
            }
        }
        return result;
    }
}
