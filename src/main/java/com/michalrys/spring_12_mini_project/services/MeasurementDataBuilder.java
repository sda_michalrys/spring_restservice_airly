package com.michalrys.spring_12_mini_project.services;

import com.michalrys.spring_12_mini_project.persistence.MeasurementResult;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class MeasurementDataBuilder {

    public MeasurementResult buildDataEntity(Map<String, Double> sourceData) {
        MeasurementResult measurementResult = new MeasurementResult();
        measurementResult.setHumidity(sourceData.get("HUMIDITY"));
        measurementResult.setPressure(sourceData.get("PRESSURE"));
        measurementResult.setTemperature(sourceData.get("TEMPERATURE"));
        measurementResult.setPm1(sourceData.get("PM1"));
        measurementResult.setPm10(sourceData.get("PM10"));
        measurementResult.setPm25(sourceData.get("PM25"));
        return measurementResult;
    }
}
